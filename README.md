# Siemens Challenge

## Setup

### Git Clone
```bash
git clone https://mtmoreira-98@bitbucket.org/mtmoreira-98/siemenscountries.git
```

### Install Maven
[Maven install guide](https://www.baeldung.com/install-maven-on-windows-linux-mac)

---
## Run Server
```
mvn spring-boot:run
```

## Run Vue App
```
cd countriesClient
npm run dev
```

## Main Features
Call external [Countrylayer API](https://countrylayer.com/documentation/)

Spring Boot Server

Vue Client Application

SSL connection

Tests Pipeline

## Main Limitations
Limited Requests Rate (around 50)

Mittigated by the use of cache


### Youtube Demo
[Youtube Demo](https://youtu.be/gEO7jvPlctM)


