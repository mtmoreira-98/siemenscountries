package com.example.countries.entity;

import java.util.List;


public class Country {

    private String name;

    private String alpha2Code;

    private String capital;

    private String region;

    private List<String> callingCodes;

    public Country(String name, String alpha2Code, List<String> callingCodes, String capital, String region) {
        this.name = name;
        this.alpha2Code = alpha2Code;
        this.callingCodes = callingCodes;
        this.capital = capital;
        this.region = region;

    }

    public Country(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlpha2Code() {
        return alpha2Code;
    }

    public void setAlpha2Code(String alpha2Code) {
        this.alpha2Code = alpha2Code;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public List<String> getCallingCodes() {
        return callingCodes;
    }

    public void setCallingCodes(List<String> callingCodes) {
        this.callingCodes = callingCodes;
    }

    @Override
    public String toString() {
        return "Country{" +
                "name='" + name + '\'' +
                ", alpha2Code='" + alpha2Code + '\'' +
                ", capital='" + capital + '\'' +
                ", region='" + region + '\'' +
                ", callingCodes=" + callingCodes +
                '}';
    }
}
