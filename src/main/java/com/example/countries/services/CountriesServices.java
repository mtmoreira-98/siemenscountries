package com.example.countries.services;

import com.example.countries.entity.Country;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CountriesServices {

    private final String urlAll = "http://api.countrylayer.com/v2/all";
    private final String urlCountry = "http://api.countrylayer.com/v2/name/";
    private final String urlRegion = "http://api.countrylayer.com/v2/region/";
    private final String urlAlpha = "http://api.countrylayer.com/v2/alpha/";

    RestTemplate restTemplate = new RestTemplate();
    ObjectMapper mapper = new ObjectMapper();



    @Value("${api-key}")
    private String apiKey;


    @Cacheable("countries")
    public List<Country> getAllCountries(){
        return getResponsesAPI(urlAll);

    }

    public Country getCountry(String name){
        String url = urlCountry + name;
        return getResponsesAPI(url).get(0);

    }

    public Country getCountryByAlpha(String alpha){
        String url = urlAlpha + alpha;
        return getResponseAPI(url);

    }

    @Cacheable("regions")
    public List<Country> getCountriesByRegion(String region){
        String url = urlRegion + region;
        return getResponsesAPI(url);
    }


    private List<Country> getResponsesAPI(String url){

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<?> entity = new HttpEntity<>(headers);

        String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("access_key", apiKey)
                .queryParam("filters", "name;alpha2Code;callingCodes;capital;region")
                .encode()
                .toUriString();

        Map<String, String> params = new HashMap<>();
        params.put("access_key", apiKey);
        params.put("filters", "name;alpha2Code;callingCodes;capital;region");


        ResponseEntity<Country[]> response = restTemplate.exchange(
                urlTemplate,
                HttpMethod.GET,
                entity,
                Country[].class,
                params
        );

        Country[] countries = response.getBody();

        return Arrays.stream(countries)
                .map(object -> mapper.convertValue(object, Country.class))
                .collect(Collectors.toList());
    }

    private Country getResponseAPI(String url){

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<?> entity = new HttpEntity<>(headers);

        String urlTemplate = UriComponentsBuilder.fromHttpUrl(url)
                .queryParam("access_key", apiKey)
                .queryParam("filters", "name;alpha2Code;callingCodes;capital;region")
                .encode()
                .toUriString();

        Map<String, String> params = new HashMap<>();
        params.put("access_key", apiKey);
        params.put("filters", "name;alpha2Code;callingCodes;capital;region");


        ResponseEntity<Country> response = restTemplate.exchange(
                urlTemplate,
                HttpMethod.GET,
                entity,
                Country.class,
                params
        );


        return response.getBody();
    }






}
