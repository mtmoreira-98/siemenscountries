package com.example.countries.controller;

import com.example.countries.entity.Country;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface CountriesAPI {

    @GetMapping("/countries")
    public ResponseEntity<List<Country>> getAllCountries();

    @GetMapping("/countries/name/{name}")
    public ResponseEntity<Country> getCountry(@PathVariable String name);

    @GetMapping("/countries/{alpha}")
    public ResponseEntity<Country> getCountryByCode(@PathVariable String alpha) throws Exception;

    @GetMapping("/region/{region}")
    public ResponseEntity<List<Country>> getCountriesByRegion(@PathVariable String region);
}
