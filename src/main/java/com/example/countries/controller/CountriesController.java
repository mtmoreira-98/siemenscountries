package com.example.countries.controller;
import com.example.countries.entity.Country;
import com.example.countries.services.CountriesServices;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@RestController
@CrossOrigin({"http://localhost:3000", "https://localhost:3000"})
@RequestMapping("/")
public class CountriesController implements CountriesAPI {


    private CountriesServices countriesServices;

    CountriesController( @Autowired CountriesServices countriesServices){
        this.countriesServices = countriesServices;
    }



    @GetMapping("/countries")
    public ResponseEntity<List<Country>> getAllCountries(){
        List<Country> countries = countriesServices.getAllCountries();
        return ResponseEntity.status(HttpStatus.OK).body(countries);
    }


    @GetMapping("/countries/name/{name}")
    public ResponseEntity<Country> getCountry(@PathVariable String name){
        Country country = countriesServices.getCountry(name);
        return  ResponseEntity.status(HttpStatus.OK).body(country);

    }


    @GetMapping("/countries/{alpha}")
    public ResponseEntity<Country> getCountryByCode(@PathVariable String alpha) throws Exception {
        Country country = countriesServices.getCountryByAlpha(alpha);
        if (country == null)
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        return  ResponseEntity.status(HttpStatus.OK).body(country);
    }

    @GetMapping("/region/{region}")
    public ResponseEntity<List<Country>> getCountriesByRegion(@PathVariable String region){
       List<Country> countries = countriesServices.getCountriesByRegion(region);
       if (countries.isEmpty())
           return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
       return  ResponseEntity.status(HttpStatus.OK).body(countries);
    }


}
