package com.example.countries;

import com.example.countries.entity.Country;
import com.example.countries.services.CountriesServices;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;



import java.util.ArrayList;
import java.util.List;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class CountriesApplicationTests {

    @MockBean
    private CountriesServices countriesServices;
    @Autowired
    private MockMvc mvc;

    ObjectMapper mapper = new ObjectMapper();


    List<String> callingCodes = new ArrayList<String>(List.of("351"));
    Country portugal = new Country("Portugal","PT",callingCodes,"Lisbon","Europe");





    @Test
    void getCountrySuccess() throws Exception {
        Mockito.when(countriesServices.getCountryByAlpha("PT")).thenReturn(portugal);
        var result = mvc.perform(get("/countries/PT"))
                .andExpect(status().is2xxSuccessful())
                .andReturn();
    }

    @Test
    void getCountryNotFound() throws Exception {
        Mockito.when(countriesServices.getCountryByAlpha("XXXXX")).thenReturn(null);
        mvc.perform(get("/countries/XXXXX"))
                .andExpect(status().isNotFound());

    }

    @Test
    void getRegionNotFound() throws Exception {
        mvc.perform(get("/region/XXXXX"))
                .andExpect(status().isNotFound());

    }
    /*
    @Test
    void getRegionSuccess() throws Exception {
        mvc.perform(get("/region/Europe"))
                .andExpect(status().is2xxSuccessful());

    }
    */
    @Test
    void getAllSuccess() throws Exception {
        mvc.perform(get("/countries/"))
                .andExpect(status().is2xxSuccessful());

    }

}
