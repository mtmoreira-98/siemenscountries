import { createRouter, createWebHistory} from 'vue-router'
import CountriesList from "../components/CountriesList.vue";
import CountryDetails from "../components/CountryDetails.vue";
import RegionList from "../components/RegionList.vue";

const routes=[
    {
        path : '/',
        name : 'CountriesList',
        component: CountriesList
    },
    {
        path : '/countries/:id',
        name : 'CountryDetails',
        component: CountryDetails,
        params : true

    },
    {
        path : '/region/:id',
        name : 'RegionList',
        component: RegionList,
        params : true

    }

]

const router = createRouter({
    history : createWebHistory(), routes
})

export default  router