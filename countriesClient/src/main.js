import { createApp } from 'vue'
import App from './App.vue'
import router from "./router";
import Dropdown from 'vue-simple-search-dropdown';


createApp(App).use(router,Dropdown).mount('#app')
